//
//  FlavorList.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct FlavorList: View {
    let flavors : [Flavors]
    var filter : String = ""
    var body: some View {
        withAnimation {
            ZStack {
                if filter == "" {
                    withAnimation {
                        List(flavors) { flavor in
                            withAnimation {
                                HStack {
                                    Text(flavor.flavor).subheadheavy()
                                    Spacer()
                                    Text(flavor.group).subheadheavy().foregroundColor(Color(flavor.color))
                                }
                            }.animation(.default)
                        }.fixList()
                    }.animation(.default)
                } else {
                    withAnimation {
                        List(flavors.filter({
                            $0.group == filter
                        })) { flavor in
                            withAnimation {
                                HStack {
                                    Text(flavor.flavor).subheadheavy()
                                    Spacer()
                                    Text(flavor.group).subheadheavy().foregroundColor(Color(flavor.color))
                                }
                            }.animation(.default)
                        }.fixList()
                    }.animation(.default)
                }
            }
        }.animation(.default)
    }
}
