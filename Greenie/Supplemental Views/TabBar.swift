//
//  TabBar.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/20/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct TabBar: View {
    @Binding var index: Int
    var body: some View {
        HStack(spacing: 10) {
            HStack {
                Image(systemName: "leaf.arrow.circlepath")
                    .resizable()
                    .frame(width: 22.5, height: 25)
                Text(self.index == 0 ? "Home" : "").fontWeight(.medium).font(.system(size: 14))
            }.padding(15)
                .background(self.index == 0 ? Color.green.opacity(0.5) : Color.clear)
                .clipShape(Capsule()).onTapGesture {
                    self.index = 0
            }
            HStack {
                Image(systemName: "icloud")
                    .resizable()
                    .frame(width: 27.5, height: 20)
                Text(self.index == 1 ? "Flavors" : "").fontWeight(.medium).font(.system(size: 14))
            }.padding(15)
                .background(self.index == 1 ? Color.green.opacity(0.5) : Color.clear)
                .clipShape(Capsule()).onTapGesture {
                    self.index = 1
            }
            HStack {
                Image(systemName: "magnifyingglass")
                    .resizable()
                    .frame(width: 20, height: 20)
                Text(self.index == 2 ? "Search" : "").fontWeight(.medium).font(.system(size: 14))
            }.padding(15)
                .background(self.index == 2 ? Color.green.opacity(0.5) : Color.clear)
                .clipShape(Capsule()).onTapGesture {
                    self.index = 2
            }
        }.padding(.top, 8)
            .frame(width: UIScreen.main.bounds.width)
            .background(Color.white)
            .animation(.default)
    }
}
