//
//  SearchList.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct SearchList: View {
    let strains : [Strains]
    let flavors : [Flavors]
    let effects : [Effects]
    @State private var search = ""
    @State private var showingDetail = false
    var body: some View {
        withAnimation {
            VStack(alignment: .leading, spacing: 0) {
                SearchBar(text: $search)
                if search == "" {
                    Spacer()
                    HStack {
                        Spacer()
                        Image("WeedSmileOutlined")
                            .resizable()
                            .frame(width: 256, height: 256)
                        Spacer()
                    }
                    Spacer()
                    HStack {
                        Spacer()
                        Text("Strains will appear as you search!").subheadheavy()
                        Spacer()
                    }
                    Spacer()
                } else {
                    List {
                        Section(header: Text("\n\(strains.filter({$0.strain.lowercased().contains(search.lowercased())}).count) Results").subheadheavy().padding(.vertical, 7.5).fixCase()) {
                            ForEach(strains.filter({
                                $0.strain.lowercased().contains(search.lowercased())
                            })) { strain in
                                withAnimation {
                                    NavigationLink(destination: StrainDetails(strain: strain, flavors: self.flavors, effects: self.effects).navigationBarTitle("\(strain.strain)")) {
                                        HStack {
                                            Text(strain.strain).subheadheavy().foregroundColor(.primary)
                                            Text("★ \(strain.ratingStr)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                                            Spacer()
                                            Text(strain.type.capitalized).subheadheavy().foregroundColor(Color(strain.color))
                                        }.padding(.vertical, 5)
                                    }
                                }.animation(.default)
                            }
                        }
                    }.fixList()
                }
            }
        }.animation(.default)
    }
}
