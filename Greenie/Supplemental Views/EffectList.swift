//
//  EffectsList.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct EffectList: View {
    let effects : [Effects]
    var body: some View {
        withAnimation {
            List(effects) { effect in
                withAnimation {
                    HStack {
                        Text(effect.effect).subheadheavy()
                        Spacer()
                        Text(effect.type.capitalized).subheadheavy().foregroundColor(Color(effect.color))
                    }
                }.animation(.default)
            }
        }.animation(.default)
    }
}
