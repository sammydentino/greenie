//
//  StrainDetails.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct StrainDetails: View {
    let strain : Strains
    let flavors : [Flavors]
    let effects : [Effects]
    var body: some View {
        withAnimation {
            List {
                withAnimation {
                    HStack {
                        Text(strain.type.capitalized).subheadheavy().foregroundColor(Color(strain.color))
                        Spacer()
                        Text("★ \(strain.ratingStr)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                    }.makeEmptySection()
                }.animation(.default)
                Section(header: Text("Description").subheadheavy().fixCase()) {
                    withAnimation {
                        Text(strain.description).font(.system(size: 15, weight: .bold, design: .rounded)).padding(.vertical, 5)
                    }.animation(.default)
                }
                Section(header: Text("Flavors").subheadheavy().fixCase()) {
                    withAnimation {
                        ForEach(strain.flavors) { flavor in
                            ForEach(self.flavors) { flavor2 in
                                if flavor.uppercased() == flavor2.flavor.uppercased() {
                                    HStack {
                                        Text(flavor).subheadheavy().padding(.vertical, 5)
                                        Spacer()
                                        Text(flavor2.group).subheadheavy().foregroundColor(Color(flavor2.color)).padding(.vertical, 5)
                                    }
                                }
                            }
                        }
                    }.animation(.default)
                }
                Section(header: Text("Effects").subheadheavy().fixCase()) {
                    withAnimation {
                        ForEach(strain.alleffects) { effect in
                            ForEach(self.effects) { effect2 in
                                if effect.uppercased() == effect2.effect.uppercased() {
                                    HStack {
                                        Text(effect).subheadheavy().padding(.vertical, 5)
                                        Spacer()
                                        Text(effect2.type.capitalized).subheadheavy().foregroundColor(Color(effect2.color)).padding(.vertical, 5)
                                    }
                                }
                            }
                        }
                    }.animation(.default)
                }
            }.fixList()
        }.animation(.default)
    }
}
