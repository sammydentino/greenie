//
//  StrainList.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct StrainList: View {
    let strains : [Strains]
    var filter : String = ""
    let flavors : [Flavors]
    let effects : [Effects]
    var body: some View {
        withAnimation {
            VStack(alignment: .leading, spacing: 0) {
                Text(" ").padding(-10)
                List(strains) { strain in
                    withAnimation {
                        NavigationLink(destination: StrainDetails(strain: strain, flavors: self.flavors, effects: self.effects).navigationBarTitle("\(strain.strain)")) {
                            HStack {
                                Text(strain.strain).subheadheavy().foregroundColor(.primary)
                                Text("★ \(strain.ratingStr)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                                Spacer()
                                Text(strain.type.capitalized).subheadheavy().foregroundColor(Color(strain.color))
                            }.padding(.vertical, 5)
                        }
                    }
                }.fixList()
            }.background(Color.systemGroupedBackground)
        }
    }
}
