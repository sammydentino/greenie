//
//  ContentView.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/8/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var data = CannabisData()
    @State private var selected = 0
    @State private var selectedeffect = 0
    @State private var selectedflavor = 0
    @State private var tagged = 0
    
    var body: some View {
        NavigationView {
            VStack {
                ZStack {
                    if self.selected == 0 {
                        home
                    } else if self.selected == 1 {
                        flavors
                    } else if self.selected == 2 {
                        search
                    }
                }.animation(.default)
                TabBar(index: $selected)
            }
        }.navigationBarColor(.darkGray)
    }
    
    var home: some View {
        List {
            HStack {
                Spacer()
                Image("WeedSmileOutlined")
                    .resizable()
                    .frame(width: 224, height: 224)
                Spacer()
            }.padding(.vertical).makeEmptySection()
            Section(header: Text("Strains").subheadheavy().foregroundColor(Color(.systemGreen)).fixCase()) {
                NavigationLink(destination: StrainList(strains: self.data.strains, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("All Strains")) {
                    HStack(alignment: .center) {
                        Text("All").subheadheavy().foregroundColor(.primary)
                        Spacer()
                        Text("\(self.data.strains.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                    }
                }
                NavigationLink(destination: StrainList(strains: self.data.sativas, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("Sativas")) {
                    HStack(alignment: .center) {
                        Text("Sativas").subheadheavy().foregroundColor(.primary)
                        Spacer()
                        Text("\(self.data.sativas.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                    }
                }
                NavigationLink(destination: StrainList(strains: self.data.hybrids, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("Hybrids")) {
                    HStack(alignment: .center) {
                        Text("Hybrids").subheadheavy().foregroundColor(.primary)
                        Spacer()
                        Text("\(self.data.hybrids.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                    }
                }
                NavigationLink(destination: StrainList(strains: self.data.indicas, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("Indicas")) {
                    HStack(alignment: .center) {
                        Text("Indicas").subheadheavy().foregroundColor(.primary)
                        Spacer()
                        Text("\(self.data.indicas.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                    }
                }
                NavigationLink(destination: StrainList(strains: self.data.fivestar, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("Top Rated")) {
                    HStack(alignment: .center) {
                        Text("Top Rated").subheadheavy().foregroundColor(.primary)
                        Spacer()
                        Text("\(self.data.fivestar.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                    }
                }
            }
        }.fixList().navigationBarTitle("Home")
    }
    
    var flavors: some View {
        VStack(alignment: .leading, spacing: 0) {
            Picker("", selection: self.$selectedflavor) {
                Text("All").tag(0)
                Text("Cool").tag(1)
                Text("Fruity").tag(2)
                Text("Natural").tag(3)
                Text("Strong").tag(4)
                Text("Sweet").tag(5)
            }.pickerStyle(SegmentedPickerStyle()).padding(10)
            if self.selectedflavor == 0 {
                FlavorList(flavors: self.data.allflavors)
                    .navigationBarTitle("All Flavors")
            } else if self.selectedflavor == 1 {
                FlavorList(flavors: self.data.allflavors, filter: "Cool")
                    .navigationBarTitle("Cool")
            } else if self.selectedflavor == 2 {
                FlavorList(flavors: self.data.allflavors, filter: "Fruity")
                    .navigationBarTitle("Fruity")
            } else if self.selectedflavor == 3 {
                FlavorList(flavors: self.data.allflavors, filter: "Natural")
                    .navigationBarTitle("Natural")
            } else if self.selectedflavor == 4 {
                FlavorList(flavors: self.data.allflavors, filter: "Strong")
                    .navigationBarTitle("Strong")
            } else if self.selectedflavor == 5 {
                FlavorList(flavors: self.data.allflavors, filter: "Sweet")
                    .navigationBarTitle("Sweet")
            }
        }.onAppear(perform: {
            self.selectedflavor = 0
        })
    }
    
    var search: some View {
        SearchList(strains: data.strains, flavors: data.allflavors, effects: data.alleffects)
            .navigationBarTitle("Search")
    }
}

/*struct ContentView2: View {
    @ObservedObject var data = CannabisData()
    @State private var selected = 0
    @State private var selectedeffect = 0
    @State private var selectedflavor = 0
    @State private var tagged = 0
    @State private var showingDetail = false
    
    let coloredNavAppearance = UINavigationBarAppearance()
    init() {
        let design = UIFontDescriptor.SystemDesign.rounded
        let descriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .largeTitle).withDesign(design)!
        let font = UIFont.init(descriptor: descriptor, size: 48)
        UINavigationBar.appearance().largeTitleTextAttributes = [.font : font.bold()]
        coloredNavAppearance.configureWithOpaqueBackground()
        coloredNavAppearance.backgroundColor = .darkGray
        coloredNavAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        coloredNavAppearance.largeTitleTextAttributes = [.font : font.bold(), .foregroundColor: UIColor.white]

        UINavigationBar.appearance().standardAppearance = coloredNavAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredNavAppearance
        UITableView().keyboardDismissMode = .onDrag
    }
    
    var body: some View {
        TabView(selection: $tagged) {
            NavigationView {
                withAnimation {
                    VStack {
                        List {
                            HStack {
                                Spacer()
                                Image("WeedSmileOutlined")
                                    .resizable()
                                    .frame(width: 256, height: 256)
                                Spacer()
                            }.padding(.vertical)
                            Section(header: Text("Strains").subheadheavy().foregroundColor(Color(.systemGreen))) {
                                Button(action: {
                                    self.showingDetail.toggle()
                                    self.coloredNavAppearance.backgroundColor = .darkGray
                                }) {
                                    HStack(alignment: .center) {
                                        Text("All").subheadheavy().foregroundColor(.primary)
                                        Spacer()
                                        Text("\(self.data.strains.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                                        
                                        Text("→").subheadheavy().foregroundColor(.primary)
                                    }
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        StrainList(strains: self.data.strains, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("All Strains")
                                    }.onDisappear(perform: {
                                        self.coloredNavAppearance.backgroundColor = .darkGray
                                    })
                                }
                                Button(action: {
                                    self.showingDetail.toggle()
                                    self.coloredNavAppearance.backgroundColor = UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)
                                }) {
                                    HStack(alignment: .center) {
                                        Text("Sativa").subheadheavy().foregroundColor(.primary)
                                        Spacer()
                                        Text("\(self.data.sativas.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                                        Text("→").subheadheavy().foregroundColor(.primary)
                                    }
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        StrainList(strains: self.data.sativas, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("Sativa")
                                    }.onDisappear(perform: {
                                        self.coloredNavAppearance.backgroundColor = .darkGray
                                    })
                                }
                                Button(action: {
                                    self.showingDetail.toggle()
                                    self.coloredNavAppearance.backgroundColor = .systemGreen
                                }) {
                                    HStack(alignment: .center) {
                                        Text("Hybrid").subheadheavy().foregroundColor(.primary)
                                        Spacer()
                                        Text("\(self.data.hybrids.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                                        Text("→").subheadheavy().foregroundColor(.primary)
                                    }.onDisappear(perform: {
                                        self.coloredNavAppearance.backgroundColor = .darkGray
                                    })
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        StrainList(strains: self.data.hybrids, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("Hybrid")
                                    }
                                }
                                Button(action: {
                                    self.showingDetail.toggle()
                                    self.coloredNavAppearance.backgroundColor = .systemPurple
                                }) {
                                    HStack(alignment: .center) {
                                        Text("Indica").subheadheavy().foregroundColor(.primary)
                                        Spacer()
                                        Text("\(self.data.indicas.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                                        Text("→").subheadheavy().foregroundColor(.primary)
                                    }.onDisappear(perform: {
                                        self.coloredNavAppearance.backgroundColor = .darkGray
                                    })
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        StrainList(strains: self.data.indicas, flavors: self.data.allflavors, effects: self.data.alleffects).navigationBarTitle("Indica")
                                    }
                                }
                                Button(action: {
                                    self.showingDetail.toggle()
                                    self.coloredNavAppearance.backgroundColor = .systemPurple
                                }) {
                                    HStack(alignment: .center) {
                                        Text("Five Star Rated Strains").subheadheavy().foregroundColor(.primary)
                                        Spacer()
                                        Text("\(self.data.fivestar.count)").font(.system(size: 12, weight: .heavy, design: .rounded)).foregroundColor(.secondary)
                                        Text("→").subheadheavy().foregroundColor(.primary)
                                    }.onDisappear(perform: {
                                        self.coloredNavAppearance.backgroundColor = .darkGray
                                    })
                                }.sheet(isPresented: self.$showingDetail) {
                                    NavigationView {
                                        StrainList(strains: self.data.fivestar, flavors: self.data.allflavors, effects: self.data.alleffects)
                                        .navigationBarTitle("Popular Strains")
                                    }
                                }
                            }
                        }.listStyle(GroupedListStyle()).navigationBarTitle("Greenie")
                    }
                }.animation(.default)
            }.navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                Image(systemName: "leaf.arrow.circlepath")
                Text("Greenie")
            }.tag(0)
            NavigationView {
                withAnimation {
                    VStack {
                        StrainList(strains: self.data.fivestar, flavors: self.data.allflavors, effects: self.data.alleffects, coloredNavAppearance: coloredNavAppearance)
                            .navigationBarTitle("Popular Strains")
                    }.onDisappear(perform: {
                        self.coloredNavAppearance.backgroundColor = .darkGray
                    })
                }.animation(.default)
            }.navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                Image(systemName: "star")
                Text("Popular")
            }.tag(1)
            NavigationView {
                withAnimation {
                    VStack(alignment: .leading, spacing: 0) {
                        Picker("", selection: self.$selectedeffect) {
                            Text("All").tag(0)
                            Text("Positive").tag(1)
                            Text("Medical").tag(2)
                            Text("Negative").tag(3)
                        }.pickerStyle(SegmentedPickerStyle()).padding(7.5)
                        if self.selectedeffect == 0 {
                            EffectList(effects: self.data.alleffects)
                                .navigationBarTitle("All Effects")
                        } else if self.selectedeffect == 1 {
                            EffectList(effects: self.data.positive)
                                .navigationBarTitle("Positive")
                        } else if self.selectedeffect == 2 {
                            EffectList(effects: self.data.medical)
                                .navigationBarTitle("Medical")
                        } else if self.selectedeffect == 3 {
                            EffectList(effects: self.data.negative)
                                .navigationBarTitle("Negative")
                        }
                    }.onAppear(perform: {
                        self.selectedeffect = 0
                    })
                }.animation(.default)
            }.navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                Image(systemName: "icloud")
                Text("Effects")
            }.tag(2)
            NavigationView {
                withAnimation {
                    VStack(alignment: .leading, spacing: 0) {
                        Picker("", selection: self.$selectedflavor) {
                            Text("All").tag(0)
                            Text("Cool").tag(1)
                            Text("Fruity").tag(2)
                            Text("Natural").tag(3)
                            Text("Strong").tag(4)
                            Text("Sweet").tag(5)
                        }.pickerStyle(SegmentedPickerStyle()).padding(7.5)
                        if self.selectedflavor == 0 {
                            FlavorList(flavors: self.data.allflavors)
                                .navigationBarTitle("All Flavors")
                        } else if self.selectedflavor == 1 {
                            FlavorList(flavors: self.data.allflavors, filter: "Cool")
                                .navigationBarTitle("Cool")
                        } else if self.selectedflavor == 2 {
                            FlavorList(flavors: self.data.allflavors, filter: "Fruity")
                                .navigationBarTitle("Fruity")
                        } else if self.selectedflavor == 3 {
                            FlavorList(flavors: self.data.allflavors, filter: "Natural")
                                .navigationBarTitle("Natural")
                        } else if self.selectedflavor == 4 {
                            FlavorList(flavors: self.data.allflavors, filter: "Strong")
                                .navigationBarTitle("Strong")
                        } else if self.selectedflavor == 5 {
                            FlavorList(flavors: self.data.allflavors, filter: "Sweet")
                                .navigationBarTitle("Sweet")
                        }
                    }.onAppear(perform: {
                        self.selectedflavor = 0
                    })
                }.animation(.default)
            }.navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                Image(systemName: "cart")
                Text("Flavors")
            }.tag(3)
            NavigationView {
                withAnimation {
                    VStack {
                        SearchList(strains: data.strains, flavors: data.allflavors, effects: data.alleffects, coloredNavAppearance: coloredNavAppearance)
                            .navigationBarTitle("Search")
                            .onDisappear(perform: {
                                self.coloredNavAppearance.backgroundColor = .darkGray
                        })
                    }
                }.animation(.default)
            }.navigationViewStyle(StackNavigationViewStyle())
                .tabItem {
                Image(systemName: "magnifyingglass")
                Text("Search")
            }.tag(4)
        }
    }
}*/

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
