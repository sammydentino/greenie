//
//  Flavors.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct Flavors : Codable, Identifiable {
    let id = UUID()
    let flavor : String!
    let group : String!
    var color : UIColor!

    enum CodingKeys: String, CodingKey {
        case flavor = "flavor"
        case group = "group"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        flavor = try values.decodeIfPresent(String.self, forKey: .flavor) ?? "N/A"
        group = try values.decodeIfPresent(String.self, forKey: .group) ?? "N/A"
        if(group == "Cool") {
            color = UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)
        } else if(group == "Natural") {
            color = .systemGreen
        } else if(group == "Sweet") {
            color = .systemPurple
        } else if(group == "Strong") {
            color = .orange
        } else if(group == "Fruity") {
            color = .systemPink
        } else {
            color = .blue
        }
    }
}
