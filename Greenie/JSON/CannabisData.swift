//
//  GetStrains.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

class CannabisData: ObservableObject {
    @Published var strains: [Strains]!
    @Published var sativas = [Strains]()
    @Published var hybrids = [Strains]()
    @Published var indicas = [Strains]()
    @Published var ratingsort = [Strains]()
    @Published var fivestar = [Strains]()
    @Published var alleffects : [Effects]!
    @Published var positive = [Effects]()
    @Published var negative = [Effects]()
    @Published var medical = [Effects]()
    @Published var allflavors : [Flavors]!
    
    init() {
        loadStrains()
        strains = strains.sorted(by: {
            $0.strain < $1.strain
        })
        for item in strains {
            if item.strain[item.strain.startIndex].isNumber == true {
                strains.removeFirst()
                strains.append(item)
            }
        }
        //strains = strains.uniqueElements()
        ratingsort = strains.sorted(by: {
            $0.rating > $1.rating
        })
        fivestar = strains.filter({
            $0.rating == 5.0
        })
        loadEffects()
        alleffects = alleffects.sorted(by: {
            $0.effect < $1.effect
        })
        for item in alleffects! {
            if item.type == "positive" {
                positive.append(item)
            } else if item.type == "medical" {
                medical.append(item)
            } else if item.type == "negative" {
                negative.append(item)
            }
        }
        for item in strains! {
            if item.type == "sativa" {
                sativas.append(item)
            } else if item.type == "hybrid" {
                hybrids.append(item)
            } else if item.type == "indica" {
                indicas.append(item)
            }
        }
        loadFlavors()
        allflavors = allflavors.sorted(by: {
            $0.flavor < $1.flavor
        })
    }
    
    func loadStrains() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Cannabis-Strains/master/Strains.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([Strains].self, from: d) {
                    strains = data
                }
            }
        }
    }
    
    func loadEffects() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Cannabis-Strains/master/effects.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([Effects].self, from: d) {
                    alleffects = data
                }
            }
        }
    }
    
    func loadFlavors() {
        let urlString = "https://raw.githubusercontent.com/sammydentino/Cannabis-Strains/master/Flavors.json"
        if let url = URL(string: urlString) {
            if let d = try? Data(contentsOf: url) {
                // we're OK to parse!
                let decoder = JSONDecoder()
                if let data = try? decoder.decode([Flavors].self, from: d) {
                    allflavors = data
                }
            }
        }
    }
}
