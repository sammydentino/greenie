//
//  Strains.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct Strains : Codable, Identifiable, Hashable {
    let id = UUID()
    var strain : String!
    let type : String!
    let rating : Double!
    var ratingStr = ""
    var effects : String!
    var flavor : String!
    let description : String!
    var color : UIColor!
    var flavors = [String]()
    var alleffects = [String]()
    var desc = [String]()

    enum CodingKeys: String, CodingKey {
        case strain = "Strain"
        case type = "Type"
        case rating = "Rating"
        case effects = "Effects"
        case flavor = "Flavor"
        case description = "Description"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        strain = try values.decodeIfPresent(String.self, forKey: .strain) ?? "N/A"
        type = try values.decodeIfPresent(String.self, forKey: .type) ?? "N/A"
        rating = try values.decodeIfPresent(Double.self, forKey: .rating) ?? 5.0
        effects = try values.decodeIfPresent(String.self, forKey: .effects) ?? "N/A"
        flavor = try values.decodeIfPresent(String.self, forKey: .flavor) ?? "N/A"
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? "N/A"
        strain = strain.replacingOccurrences(of: "-", with: " ")
        strain = strain.replacingOccurrences(of: "Og", with: "OG")
        strain = strain.replacingOccurrences(of: "Ak", with: "AK")
        strain = strain.replacingOccurrences(of: "A ", with: "A-")
        strain = strain.replacingOccurrences(of: "Acdc", with: "ACDC")
        strain = strain.replacingOccurrences(of: "Bb", with: "BB")
        strain = strain.replacingOccurrences(of: "Bcn", with: "BCN")
        strain = strain.replacingOccurrences(of: "Bc", with: "BC")
        strain = strain.replacingOccurrences(of: "Cbd", with: "CBD")
        strain = strain.replacingOccurrences(of: "Lamb S", with: "Lamb's")
        strain = strain.replacingOccurrences(of: "Gsc", with: "Girl Scout Cookies")
        strain = strain.replacingOccurrences(of: "Wtf", with: "WTF")
        strain = strain.replacingOccurrences(of: "Lsd", with: "LSD")
        strain = strain.replacingOccurrences(of: "Granddaddy", with: "Grand Daddy")
        effects = effects.replacingOccurrences(of: ",", with: "\n")
        flavor = flavor.replacingOccurrences(of: ",", with: "\n")
        if(type == "sativa") {
            color = UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)
        } else if(type == "hybrid") {
            color = .systemGreen
        } else if(type == "indica") {
            color = .systemPurple
        }
        flavors = flavor.components(separatedBy: "\n")
        alleffects = effects.components(separatedBy: "\n")
        desc = description.components(separatedBy: ". ")
        ratingStr = String(format: "%.1f", rating)
    }
}

extension Strains: Equatable {
    static func ==(lhs: Strains, rhs: Strains) -> Bool {
        return lhs.strain == rhs.strain && lhs.strain == rhs.strain
    }
}

