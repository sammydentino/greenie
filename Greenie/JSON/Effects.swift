//
//  Effects.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct Effects : Codable, Identifiable {
    let id = UUID()
    let effect : String!
    let type : String!
    var color : UIColor!

    enum CodingKeys: String, CodingKey {
        case effect = "effect"
        case type = "type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        effect = try values.decodeIfPresent(String.self, forKey: .effect) ?? "N/A"
        type = try values.decodeIfPresent(String.self, forKey: .type) ?? "N/A"
        if(type == "medical") {
            color = UIColor(red: 0, green: 0.7137, blue: 1.0, alpha: 1.0)
        } else if(type == "positive") {
            color = .systemGreen
        } else if(type == "negative") {
            color = .systemPurple
        }
    }
}
