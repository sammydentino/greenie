//
//  EffectsList.swift
//  Greenie
//
//  Created by Sammy Dentino on 7/16/20.
//  Copyright © 2020 Sammy Dentino. All rights reserved.
//

import SwiftUI

struct EffectList: View {
    let effects : [Effects]
    var body: some View {
        withAnimation {
            List(effects) { effect in
                withAnimation {
                    HStack {
                        Text(effect.effect).font(.system(size: 15, weight: .heavy, design: .rounded))
                        Spacer()
                        Text(effect.type.capitalized).font(.system(size: 15, weight: .heavy, design: .rounded)).foregroundColor(effect.color)
                    }
                }.animation(.default)
            }.listStyle(GroupedListStyle())
        }.animation(.default)
    }
}
